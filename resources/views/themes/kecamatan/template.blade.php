<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="id-ID" prefix="og: http://ogp.me/ns#">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="id-ID" prefix="og: http://ogp.me/ns#">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="id-ID" prefix="og: http://ogp.me/ns#">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="id-ID" prefix="og: http://ogp.me/ns#">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <title>{{$title or (isset($post) ? $post->title : 'Home') }} | Pemerintah {{globalParams('site_name')}}</title>
            
    
    <!-- font-aweosme -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="/frontend/Divi/js/html5.js" type="text/javascript"></script>
    <![endif]-->

    <script type="text/javascript">
        document.documentElement.className = 'js';
    </script>

    
<link rel="canonical" href="{{url('/')}}" />
<meta property="og:locale" content="id_ID" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$title or 'title'}}" />
<meta property="og:description" content="{{globalParams('description')}}" />
<meta property="og:url" content="{{url('/')}}" />
<meta property="og:site_name" content="{{globalParams('site_name')}}" />
<meta property="og:image" content="{{isset($post->picture) ? $post->picture : ''}}" />
<meta name="twitter:card" content="summary"/>
<meta name="twitter:description" content="{{globalParams('description')}}"/>
<meta name="twitter:title" content="{{globalParams('description')}}"/>
<meta name="twitter:domain" content="{{globalParams('description')}}"/>
<meta name="twitter:image:src" content="{{isset($post->picture) ? $post->picture : ''}}"/>
<meta content="WEB RESMI KECAMATAN KRAMAT KABUPATEN TEGAL v.2.0" name="generator"/>
<link rel='stylesheet' id='divi-fonts-css'  href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&#038;subset=latin,latin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='divi-style-css'  href='/frontend/web-resmi-kecamatan-kramat-kabupaten-tegal/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='et-shortcodes-css-css'  href='/frontend/Divi/epanel/shortcodes/css/shortcodes.css' type='text/css' media='all' />
<link rel='stylesheet' id='et-shortcodes-responsive-css-css'  href='/frontend/Divi/epanel/shortcodes/css/shortcodes_responsive.css' type='text/css' media='all' />
<script src="/frontend/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script type='text/javascript' src='/frontend/plugins/pbp-newsticker/media/pbpNewsticker/newsticker.jquery.js'></script>
<meta name="generator" content="LiveCMS v.0.1" />
<link rel='shortlink' href='{{url('/')}}' />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />        
<style id="theme-customizer-css">
        .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button, .woocommerce-message, .woocommerce-error, .woocommerce-info { background: #eeee22 !important; }
            #et_search_icon:hover, .mobile_menu_bar:before, .et-social-icon a:hover, .et_pb_sum, .et_pb_pricing li a, .et_pb_pricing_table_button, .et_overlay:before, .entry-summary p.price ins, .woocommerce div.product span.price, .woocommerce-page div.product span.price, .woocommerce #content div.product span.price, .woocommerce-page #content div.product span.price, .woocommerce div.product p.price, .woocommerce-page div.product p.price, .woocommerce #content div.product p.price, .woocommerce-page #content div.product p.price, .et_pb_member_social_links a:hover, .woocommerce .star-rating span:before, .woocommerce-page .star-rating span:before, .et_pb_widget li a:hover, .et_pb_filterable_portfolio .et_pb_portfolio_filters li a.active, .et_pb_filterable_portfolio .et_pb_portofolio_pagination ul li a.active, .et_pb_gallery .et_pb_gallery_pagination ul li a.active, .wp-pagenavi span.current, .wp-pagenavi a:hover, .nav-single a, .posted_in a { color: #eeee22; }
            .et_pb_contact_submit, .et_password_protected_form .et_submit_button, .et_pb_bg_layout_light .et_pb_newsletter_button, .comment-reply-link, .form-submit input, .et_pb_bg_layout_light .et_pb_promo_button, .et_pb_bg_layout_light .et_pb_more_button, .woocommerce a.button.alt, .woocommerce-page a.button.alt, .woocommerce button.button.alt, .woocommerce-page button.button.alt, .woocommerce input.button.alt, .woocommerce-page input.button.alt, .woocommerce #respond input#submit.alt, .woocommerce-page #respond input#submit.alt, .woocommerce #content input.button.alt, .woocommerce-page #content input.button.alt, .woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button { color: #eeee22; }
            .footer-widget h4 { color: #eeee22; }
            .et-search-form, .nav li ul, .et_mobile_menu, .footer-widget li:before, .et_pb_pricing li:before, blockquote { border-color: #eeee22; }
            .et_pb_counter_amount, .et_pb_featured_table .et_pb_pricing_heading, .et_quote_content, .et_link_content, .et_audio_content { background-color: #eeee22; }
                                    a { color: #326b1b; }
                                    #main-header .nav li ul { background-color: rgba(48,48,48,0.92); }
                            .nav li ul { border-color: #ffffff; }
                            #top-header, #et-secondary-nav li ul { background-color: #efefef; }
                                #top-header, #top-header a { color: #2b2b2b; }
                                    .et_header_style_centered .mobile_nav .select_page, .et_header_style_split .mobile_nav .select_page, .et_nav_text_color_light #top-menu > li > a, .et_nav_text_color_dark #top-menu > li > a, #top-menu a, .et_mobile_menu li a, .et_nav_text_color_light .et_mobile_menu li a, .et_nav_text_color_dark .et_mobile_menu li a, #et_search_icon:before, .et_search_form_container input, span.et_close_search_field:after, #et-top-navigation .et-cart-info, .mobile_menu_bar:before { color: #ffffff; }
            .et_search_form_container input::-moz-placeholder { color: #ffffff; }
            .et_search_form_container input::-webkit-input-placeholder { color: #ffffff; }
            .et_search_form_container input:-ms-input-placeholder { color: #ffffff; }
                                
        
                    #top-menu li.current-menu-ancestor > a, #top-menu li.current-menu-item > a { color: #eeee22; }
                            #main-footer { background-color: #000000; }
                                            #main-footer .footer-widget h4 { color: #eeee22; }
                            .footer-widget li:before { border-color: #eeee22; }
                                                                
        
        
        @media only screen and ( min-width: 981px ) {
        .et-fixed-header#top-header, .et-fixed-header#top-header #et-secondary-nav li ul { background-color: #efefef; }
        .et-fixed-header #top-menu a, .et-fixed-header #et_search_icon:before, .et-fixed-header #et_top_search .et-search-form input, .et-fixed-header .et_search_form_container input, .et-fixed-header .et_close_search_field:after, .et-fixed-header #et-top-navigation .et-cart-info { color: #ffffff !important; }
                .et-fixed-header .et_search_form_container input::-moz-placeholder { color: #ffffff !important; }
                .et-fixed-header .et_search_form_container input::-webkit-input-placeholder { color: #ffffff !important; }
                .et-fixed-header .et_search_form_container input:-ms-input-placeholder { color: #ffffff !important; }
                                        .et-fixed-header #top-menu li.current-menu-ancestor > a, .et-fixed-header #top-menu li.current-menu-item > a { color: #eeee22 !important; }
            
                    }
        @media only screen and ( min-width: 1350px) {
            .et_pb_row { padding: 27px 0; }
            .et_pb_section { padding: 54px 0; }
            .single.et_pb_pagebuilder_layout.et_full_width_page .et_post_meta_wrapper { padding-top: 81px; }
            .et_pb_section.et_pb_section_first { padding-top: inherit; }
            .et_pb_fullwidth_section { padding: 0; }
        }
        @media only screen and ( max-width: 980px ) {
                                                                                }
        @media only screen and ( max-width: 767px ) {
                                                        }
    </style>

    <style>
        .post-content {
            padding-top: 2em;
            padding-bottom: 2em;
        }

        .post-meta span {
            padding-right: 20px;
            display: inline-block;
        }
        .post-meta span .fa {
            margin-right: 5px;
        }

        .post-meta {
            padding-bottom: 20px;
        }

    </style>

@yield('css.header')

</head>
<body class="home page page-id-564 page-template page-template-page-template-home page-template-page-template-home-php custom-background et_pb_button_helper_class et_fullwidth_nav et_fixed_nav et_show_nav et_cover_background et_secondary_nav_enabled et_secondary_nav_two_panels et_pb_gutter linux et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_centered et_right_sidebar chrome">
    <div id="page-container">

            <div id="top-header">
            <div class="container clearfix">

            
                <div id="et-info">
                                    <span id="et-info-phone">{{globalParams('telephone')}}</span>
                
                                    <a href="mailto:{{globalParams('email')}}"><span id="et-info-email">{{globalParams('email')}}</span></a>
                
                <ul class="et-social-icons">

    <li class="et-social-icon et-social-facebook">
        <a href="#" class="icon">
            <span>Facebook</span>
        </a>
    </li>
    <li class="et-social-icon et-social-twitter">
        <a href="#" class="icon">
            <span>Twitter</span>
        </a>
    </li>
    <li class="et-social-icon et-social-google-plus">
        <a href="#" class="icon">
            <span>Google</span>
        </a>
    </li>
    <!-- 
    <li class="et-social-icon et-social-rss">
        <a href="{{url('feed')}}" class="icon">
            <span>RSS</span>
        </a>
    </li> -->

</ul>               </div> <!-- #et-info -->

            
                <div id="et-secondary-menu">
                <div class="et_duplicate_social_icons">
                                <ul class="et-social-icons">

    <li class="et-social-icon et-social-facebook">
        <a href="#" class="icon">
            <span>Facebook</span>
        </a>
    </li>
    <li class="et-social-icon et-social-twitter">
        <a href="#" class="icon">
            <span>Twitter</span>
        </a>
    </li>
    <li class="et-social-icon et-social-google-plus">
        <a href="#" class="icon">
            <span>Google</span>
        </a>
    </li>
    <!-- 
    <li class="et-social-icon et-social-rss">
        <a href="{{url('feed')}}" class="icon">
            <span>RSS</span>
        </a>
    </li> -->

</ul>
                            </div><ul id="et-secondary-nav" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-230"><a href="{{url('login')}}"><i class="fa fa-lock"></i> Masuk</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-643"><a href="http://tegalkab.go.id"><i class="fa fa-globe"></i> Website Pemerintah Kabupaten Tegal</a></li>
</ul>               </div> <!-- #et-secondary-menu -->

            </div> <!-- .container -->
        </div> <!-- #top-header -->
    
        <header id="main-header" data-height-onload="66">
            <div class="container clearfix et_menu_container">
                            <div class="logo_container">
                    <span class="logo_helper"></span>
                        <a href="{{url('/')}}" title="Situs Resmi Kecamatan Kramat Kabupaten Tegal - Jawa Tengah">
                            <div class="logo">
                                    <img src="/frontend/web-resmi-kecamatan-kramat-kabupaten-tegal/logo.png" alt="Situs Resmi Kecamatan Kramat Kabupaten Tegal - Jawa Tengah" id="logo" />
                            </div>
                        </a>
                        <div class="header-title">
                            <a href="{{url('/')}}" title="Situs Resmi Kecamatan Kramat Kabupaten Tegal - Jawa Tengah">
                                <h4>Pemerintah {{globalParams('site_name')}}</h4>
                                <address>
                                    Alamat : {{globalParams('address')}}, {{globalParams('city')}}, Telp : {{globalParams('telephone')}} <br> Kode POS (range) : {{globalParams('postcode')}}
                                </address>
                            </a>
                            <div class="newsticker">
                                <script>
(function($) {    
    $(document).ready(function() {
        $('#newsticker_0').newsticker({
                            'tickerTitle' : "",
                            'style' : "scroll",
                            'pauseOnHover' : true,
                            'showControls' : false,
                            'autoStart' : true,
                            'scrollSpeed' : "15",
                            'slideSpeed' : "1000",
                            'slideEasing' : "swing"                        
        });
    });    
})( jQuery );
</script>
<ul class="newsticker" id="newsticker_0">
<?php $latest = isset($latest) ? $latest : get('article', null, 10, [], ['id', 'title', 'slug'], 'desc', 'published_at'); ?>
@if (count($latest))
    @foreach ($latest as $p)
    <li>- <a href="{{$p->url}}">{{$p->title}}</a> </li>
    @endforeach
@endif
</ul>
                                    </div>
                            
                        </div>

                </div>
<div id="et-top-navigation">
<nav id="top-menu-nav">

@include('themes.kecamatan.mainmenu')

</nav>
<div id="et_top_search">
    <span id="et_search_icon"></span>
</div>
                    
<div id="et_mobile_nav_menu">
    <div class="mobile_nav closed">
        <span class="select_page">Pilih Laman</span>
        <span class="mobile_menu_bar"></span>
    </div>
</div>              </div> <!-- #et-top-navigation -->
    </div> <!-- .container -->
    <div class="et_search_outer">
        <div class="container et_search_form_container">
            <form role="search" method="get" class="et-search-form" action="{{url('/')}}">
            <input type="search" class="et-search-field" placeholder="Cari &hellip;" value="" name="s" title="Mencari:" />                  </form>
            <span class="et_close_search_field"></span>
        </div>
    </div>
</header> <!-- #main-header -->

<div id="et-main-area">
<div id="main-content">
  
@yield('homepage')  

    <div class="container">
        <div id="content-area" class="clearfix">
            <div id="left-area">

                @yield('content')
                    
            </div> <!-- #left-area -->

<div id="sidebar">
    @include('themes.kecamatan.sidebar')
</div> <!-- end #sidebar -->
</div> <!-- #content-area -->
</div> <!-- .container -->


</div> <!-- #main-content -->


    <span class="et_pb_scroll_top et-pb-icon"></span>


            <footer id="main-footer">
                
<div class="container">
    <div id="footer-widgets" class="clearfix">
    <div class="footer-widget"><div id="aboutmewidget-2" class="fwidget et_pb_widget widget_aboutmewidget"><h4 class="title">Pemerintah Kecamatan Kramat Kabupaten Tegal</h4>       <div class="clearfix">
            <img src="" id="about-image" alt="" />
            <address>
Alamat : Jl. Garuda I Kemantran, Kramat, Kabupaten Tegal, <br>Telp : (0283) 356039 <br> Kode POS (range) : 52181                                </address>      </div> <!-- end about me section -->
    </div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="nav_menu-3" class="fwidget et_pb_widget widget_nav_menu"><div class="menu-desa-1-container"><ul id="menu-desa-1" class="menu"><li id="menu-item-648" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-648"><a href="/padaharja">Desa Padaharja</a></li>
<li id="menu-item-649" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-649"><a href="/babakan">Desa Babakan</a></li>
<li id="menu-item-650" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-650"><a href="/bangun-galih">Desa Bangun Galih</a></li>
<li id="menu-item-651" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-651"><a href="/kertayasa">Desa Kertayasa</a></li>
<li id="menu-item-652" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-652"><a href="/kertaharja">Desa Kertaharja</a></li>
<li id="menu-item-653" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-653"><a href="/kepunduhan">Desa Kepunduhan</a></li>
<li id="menu-item-654" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-654"><a href="/kemuning">Desa Kemuning</a></li>
</ul></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="nav_menu-4" class="fwidget et_pb_widget widget_nav_menu"><div class="menu-desa-2-container"><ul id="menu-desa-2" class="menu"><li id="menu-item-655" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-655"><a href="/kematran">Desa Kematran</a></li>
<li id="menu-item-656" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-656"><a href="/jatilawang">Desa Jatilawang</a></li>
<li id="menu-item-657" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-657"><a href="/dinuk">Desa Dinuk</a></li>
<li id="menu-item-658" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-658"><a href="/bongkok">Desa Bongkok</a></li>
<li id="menu-item-659" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-659"><a href="/tanjung-harja">Desa Tanjung Harja</a></li>
<li id="menu-item-660" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-660"><a href="/plumbungan">Desa Plumbungan</a></li>
<li id="menu-item-661" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-661"><a href="/munjung-agung">Desa Munjung Agung</a></li>
</ul></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget last"><div id="nav_menu-5" class="fwidget et_pb_widget widget_nav_menu"><div class="menu-desa-3-container"><ul id="menu-desa-3" class="menu"><li id="menu-item-662" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-662"><a href="/mejasem-timur">Desa Mejasem Timur</a></li>
<li id="menu-item-663" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-663"><a href="/mejasem-barat">Desa Mejasem Barat</a></li>
<li id="menu-item-664" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-664"><a href="/maribaya">Desa Maribaya</a></li>
<li id="menu-item-665" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-665"><a href="/kramat">Desa Kramat</a></li>
<li id="menu-item-666" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-666"><a href="/ketileng">Desa Ketileng</a></li>
<li id="menu-item-667" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-667"><a href="/dampyak">Kelurahan Dampyak</a></li>
</ul></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget -->   </div> <!-- #footer-widgets -->
</div>  <!-- .container -->

        
                <div id="footer-bottom">
                    <div class="container clearfix">
                <ul class="et-social-icons">

    <li class="et-social-icon et-social-facebook">
        <a href="#" class="icon">
            <span>Facebook</span>
        </a>
    </li>
    <li class="et-social-icon et-social-twitter">
        <a href="#" class="icon">
            <span>Twitter</span>
        </a>
    </li>
    <li class="et-social-icon et-social-google-plus">
        <a href="#" class="icon">
            <span>Google</span>
        </a>
    </li>
    <!-- 
    <li class="et-social-icon et-social-rss">
        <a href="{{url('feed')}}" class="icon">
            <span>RSS</span>
        </a>
    </li> -->

</ul>
                        <p id="footer-info">Copyright &copy; 2016 <a href="">Kecamatan Kramat, Kabupaten Tegal</a> | Masih dalam tahap pembuatan oleh <a href="http://hiret.web.id">Hiret Web Indonesia</a></p>
                    </div>  <!-- .container -->
                </div>
            </footer> <!-- #main-footer -->
        </div> <!-- #et-main-area -->


    </div> <!-- #page-container -->

    <link rel='stylesheet' id='nlpcss-css'  href='/frontend/Divi/css/default_style.css' type='text/css' media='all' />

<script type='text/javascript' src='/frontend/web-resmi-kecamatan-kramat-kabupaten-tegal/js/custom.js'></script>
<script type='text/javascript' src='/frontend/Divi/js/jquery.fitvids.js'></script>
<script type='text/javascript' src='/frontend/Divi/js/waypoints.min.js'></script>
<script type='text/javascript' src='/frontend/Divi/js/jquery.magnific-popup.js'></script>
<script type='text/javascript' src='/frontend/Divi/js/smoothscroll.js'></script>
<!-- Child theme custom CSS created by Divi Children - http://divi4u.com/divi-children-plugin/ -->
<style type="text/css" media="screen">
 #footer-widgets {padding-top:80px;}
 .footer-widget {margin-bottom:50px!important;}
 #main-footer { background-color:#2e2e2e!important;}
 .footer-widget .title {font-size:18px;}
 #footer-bottom { background-color:#1f1f1f;}
 #footer-bottom {padding:15px 0 5px;}
 #footer-info, #footer-info a {color:#666666;}
 #footer-info, #footer-info a {font-size:14px;}
 #footer-bottom ul.et-social-icons li a {font-size:24px;}
 #footer-bottom ul.et-social-icons li {margin-left:18px;}
 #sidebar h4.widgettitle {font-size:18px;}
 #sidebar li, #sidebar li a {font-size:14px;}
 #custom_sidebar_module_1 h4.widgettitle {font-size:18px;}
 #custom_sidebar_module_1 li, #custom_sidebar_module_1 li a {font-size:14px;}
</style>
<!-- End Child theme custom CSS -->

</body>
</html>