@extends(theme('front'))

@section('homepage')

<div id="header-homepage" class="et_pb_section  et_pb_section_0 et_pb_with_background et_section_regular" style="padding: 0;">
        <div class=" et_pb_row et_pb_row_0">
                
                <div class="et_pb_column et_pb_column_4_4 et_pb_column_0">
                <div class="et_pb_promo et_pb_module et_pb_bg_layout_dark et_pb_text_align_center  et_pb_cta_0 et_pb_no_bg">
                <div class="et_pb_promo_description">
                    
            <h1 class="website-title">
            WEBSITE RESMI<br>
            PEMERINTAH KECAMATAN KRAMAT KABUPATEN TEGAL</h1>
            <div class="website-subtitle">Selamat datang di Portal Digital &amp; Forum Komunikasi<br>
            Masyarakat Kramat Kabupaten Tegal</div>


                </div>
                <!--<a class="et_pb_promo_button et_pb_button" href="/tentang-kami">Profile Kecamatan Kramat</a>-->
            </div>
            
            </div> <!-- .et_pb_column -->
                    
            </div> <!-- .et_pb_row -->
                
            </div>
            
            
            <!--DOWNLOAD-->
            <div class="et_pb_section  et_pb_section_0 et_pb_with_background et_section_regular" style="padding: 0; background-color: #333;">
            <div class=" et_pb_row et_pb_row_0" style="padding: 10px 0;">
            <div class="et_pb_column et_pb_column_4_4 et_pb_column_0">
            <div class="et_pb_promo et_pb_module et_pb_bg_layout_dark et_pb_text_align_center  et_pb_cta_0 et_pb_no_bg">
                    <h3><i class="fa fa-download fa-lg"></i> Untuk Download RPJMDes se-Kecamatan Kramat - Kabupaten Tegal <a class="et_pb_promo_button et_pb_button" href="/download">Klik di sini</a></h3>
            </div>
            </div>
            </div>
            </div>
@stop

@section('content')
    <article id="post-564" class="post-564 page type-page status-publish has-post-thumbnail hentry">

        <h1 class="main_title" style="font-weight: 600; font-size: 2em;">{{$post->title}}</h1>
        
        @if ($post->picture)
        <div class="background-img">
            <img class="img-responsive" alt="{{$post->title}}" src="{{$post->picture}}">
        </div>
        @endif

        <div class="post-content entry-content">
        
            {!!$post->content!!}
        
        </div> <!-- .entry-content -->
                                
    </article> <!-- .et_pb_post -->

@stop
@section('css.header')
    <style type="text/css">
        #header-homepage {
            background-image: url(/frontend/web-resmi-kecamatan-kramat-kabupaten-tegal/bupati_wabup_background.jpg)!important;
        }
    </style>
@stop
