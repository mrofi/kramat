<ul id="top-menu" class="nav et_disable_top_tier"><li id="menu-item-573" class="btn-special-2 menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-564 current_page_item menu-item-573"><a href="{{url('/')}}"><i class="fa fa-home fa-lg"></i> Profile Kecamatan Kramat Kabupaten Tegal</a></li>
<li id="menu-item-554" class="mega-menu menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-554"><a href="#">Profile Desa dan Kelurahan</a>
<ul class="sub-menu">
    <li id="menu-item-156" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-156"><a href="#">Desa</a>
    <ul class="sub-menu">
        <li id="menu-item-531" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-531"><a href="/padaharja">Desa Padaharja</a></li>
        <li id="menu-item-532" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-532"><a href="/babakan">Desa Babakan</a></li>
        <li id="menu-item-533" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-533"><a href="/Bangun-Galih">Desa Bangun Galih</a></li>
        <li id="menu-item-534" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-534"><a href="/kertayasa">Desa Kertayasa</a></li>
        <li id="menu-item-535" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-535"><a href="/kertaharja">Desa Kertaharja</a></li>
        <li id="menu-item-536" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-536"><a href="/kepunduhan">Desa Kepunduhan</a></li>
        <li id="menu-item-537" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-537"><a href="/kemuning">Desa Kemuning</a></li>
    </ul>
</li>
    <li id="menu-item-218" class="hide-on-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-218"><a href="#"> </a>
    <ul class="sub-menu">
        <li id="menu-item-538" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-538"><a href="/kemantran">Desa Kemantran</a></li>
        <li id="menu-item-539" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-539"><a href="/jatilawang">Desa Jatilawang</a></li>
        <li id="menu-item-540" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-540"><a href="/dinuk">Desa Dinuk</a></li>
        <li id="menu-item-541" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-541"><a href="/bongkok">Desa Bongkok</a></li>
        <li id="menu-item-542" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-542"><a href="/tanjung-harja">Desa Tanjung Harja</a></li>
        <li id="menu-item-543" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-543"><a href="/plumbungan">Desa Plumbungan</a></li>
        <li id="menu-item-544" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-544"><a href="/munjung-agung">Desa Munjung Agung</a></li>
    </ul>
</li>
    <li id="menu-item-219" class="hide-on-mobile menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-219"><a href="#"> </a>
    <ul class="sub-menu">
        <li id="menu-item-545" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-545"><a href="/mejasem-timur">Desa Mejasem Timur</a></li>
        <li id="menu-item-546" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-546"><a href="/mejasem-barat">Desa Mejasem Barat</a></li>
        <li id="menu-item-547" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-547"><a href="/maribaya">Desa Maribaya</a></li>
        <li id="menu-item-548" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-548"><a href="/kramat">Desa Kramat</a></li>
        <li id="menu-item-549" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-549"><a href="/ketileng">Desa Ketileng</a></li>
    </ul>
</li>
    <li id="menu-item-157" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-157"><a href="#">Kelurahan</a>
    <ul class="sub-menu">
        <li id="menu-item-550" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-550"><a href="/dampyak">Kelurahan Dampyak</a></li>
    </ul>
</li>
</ul>
</li>
<li id="menu-item-572" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-572"><a href="{{url(globalParams('slug_article', config('livecms.slugs.article')))}}">Berita Kecamatan</a></li>
<li id="menu-item-647" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-647"><a href="https://www-hclient3.rhcloud.com/download/">Download</a></li>
</ul>