@extends(theme('front'))

@section('content')
    <article id="post-564" class="post-564 page type-page status-publish has-post-thumbnail hentry">

        <h1 class="main_title" style="font-weight: 600; font-size: 2em;">{{$article->title}}</h1>
        
        <div class="post-meta text-muted">
            <span><i class="fa fa-user"></i> {{$article->author->name}}</span>
            <span><i class="fa fa-clock-o"></i> {{$article->published_at->diffForHumans()}}</span>
            @if (count($article->categories))
            <span><i class="fa fa-list"></i> Category: {{dataImplode($article->categories, 'category')}}</span>
            @endif
            @if (count($article->tags))
            <span><i class="fa fa-tag"></i> Tags: {{dataImplode($article->tags, 'tag')}}</span>
            @endif
        </div>
        @if ($article->picture)
        <div class="background-img">
            <img class="img-responsive" alt="{{$article->title}}" src="{{$article->picture}}">
        </div>
        @endif

        <div class="post-content entry-content">
        
            {!!$article->content!!}
        
        </div> <!-- .entry-content -->
                                
    </article> <!-- .et_pb_post -->

@stop

@section('script')
<script>
    $(function() {
        $('.articles').jscroll();
    });
</script>
@stop
