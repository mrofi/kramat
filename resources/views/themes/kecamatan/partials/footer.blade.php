            <footer id="main-footer">
                
<div class="container">
    <div id="footer-widgets" class="clearfix">
    <div class="footer-widget"><div id="aboutmewidget-2" class="fwidget et_pb_widget widget_aboutmewidget"><h4 class="title">Pemerintah Kecamatan Kramat Kabupaten Tegal</h4>       <div class="clearfix">
            <img src="" id="about-image" alt="" />
            <address>
Alamat : Jl. Garuda I Kemantran, Kramat, Kabupaten Tegal, <br>Telp : (0283) 356039 <br> Kode POS (range) : 52181                                </address>      </div> <!-- end about me section -->
    </div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="nav_menu-3" class="fwidget et_pb_widget widget_nav_menu"><div class="menu-desa-1-container"><ul id="menu-desa-1" class="menu"><li id="menu-item-648" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-648"><a href="/padaharja">Desa Padaharja</a></li>
<li id="menu-item-649" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-649"><a href="/babakan">Desa Babakan</a></li>
<li id="menu-item-650" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-650"><a href="/bangun-galih">Desa Bangun Galih</a></li>
<li id="menu-item-651" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-651"><a href="/kertayasa">Desa Kertayasa</a></li>
<li id="menu-item-652" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-652"><a href="/kertaharja">Desa Kertaharja</a></li>
<li id="menu-item-653" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-653"><a href="/kepunduhan">Desa Kepunduhan</a></li>
<li id="menu-item-654" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-654"><a href="/kemuning">Desa Kemuning</a></li>
</ul></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget"><div id="nav_menu-4" class="fwidget et_pb_widget widget_nav_menu"><div class="menu-desa-2-container"><ul id="menu-desa-2" class="menu"><li id="menu-item-655" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-655"><a href="/kematran">Desa Kematran</a></li>
<li id="menu-item-656" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-656"><a href="/jatilawang">Desa Jatilawang</a></li>
<li id="menu-item-657" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-657"><a href="/dinuk">Desa Dinuk</a></li>
<li id="menu-item-658" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-658"><a href="/bongkok">Desa Bongkok</a></li>
<li id="menu-item-659" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-659"><a href="/tanjung-harja">Desa Tanjung Harja</a></li>
<li id="menu-item-660" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-660"><a href="/plumbungan">Desa Plumbungan</a></li>
<li id="menu-item-661" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-661"><a href="/munjung-agung">Desa Munjung Agung</a></li>
</ul></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget --><div class="footer-widget last"><div id="nav_menu-5" class="fwidget et_pb_widget widget_nav_menu"><div class="menu-desa-3-container"><ul id="menu-desa-3" class="menu"><li id="menu-item-662" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-662"><a href="/mejasem-timur">Desa Mejasem Timur</a></li>
<li id="menu-item-663" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-663"><a href="/mejasem-barat">Desa Mejasem Barat</a></li>
<li id="menu-item-664" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-664"><a href="/maribaya">Desa Maribaya</a></li>
<li id="menu-item-665" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-665"><a href="/kramat">Desa Kramat</a></li>
<li id="menu-item-666" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-666"><a href="/ketileng">Desa Ketileng</a></li>
<li id="menu-item-667" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-667"><a href="/dampyak">Kelurahan Dampyak</a></li>
</ul></div></div> <!-- end .fwidget --></div> <!-- end .footer-widget -->   </div> <!-- #footer-widgets -->
</div>  <!-- .container -->

        
                <div id="footer-bottom">
                    <div class="container clearfix">
                <ul class="et-social-icons">

    <li class="et-social-icon et-social-facebook">
        <a href="#" class="icon">
            <span>Facebook</span>
        </a>
    </li>
    <li class="et-social-icon et-social-twitter">
        <a href="#" class="icon">
            <span>Twitter</span>
        </a>
    </li>
    <li class="et-social-icon et-social-google-plus">
        <a href="#" class="icon">
            <span>Google</span>
        </a>
    </li>
    <li class="et-social-icon et-social-rss">
        <a href="https://www-hclient3.rhcloud.com/feed/" class="icon">
            <span>RSS</span>
        </a>
    </li>

</ul>
                        <p id="footer-info">Copyright &copy; 2016 <a href="">Kecamatan Kramat, Kabupaten Tegal</a> | Masih dalam tahap pembuatan oleh <a href="http://hiret.web.id">Hiret Web Indonesia</a></p>
                    </div>  <!-- .container -->
                </div>
            </footer> <!-- #main-footer -->
