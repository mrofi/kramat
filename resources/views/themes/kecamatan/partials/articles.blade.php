@foreach ($articles as $article)

    <article id="post-{{$article->id}}" class="et_pb_post post-{{$article->id}} post type-post status-publish format-standard has-post-thumbnail hentry category-artikel">

        <a href="{{$article->url}}">
            <div class="img" style="background-image: url({{$article->picture ?: '//placehold.it/1080?text='.str_slug($article->title, '+')}})" alt="{{$article->title}}" width="1080"></div>
        </a>
                    
        <h2><a href="{{$article->url}}">{{$article->title}}</a></h2>
        
        <div class="post-meta text-muted">
            <span><i class="fa fa-clock-o"></i> {{$article->published_at->diffForHumans()}}</span>
            @if (count($article->tags))
            <span><i class="fa fa-tag"></i> Tags: {{dataImplode($article->tags, 'tag')}}</span>
            @endif
        </div>
        
        <p>
            {{$article->highlight}}
        </p>

    </article> <!-- .et_pb_post -->

@endforeach