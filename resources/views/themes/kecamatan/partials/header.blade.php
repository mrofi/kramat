            <div id="top-header">
            <div class="container clearfix">

            
                <div id="et-info">
                                    <span id="et-info-phone">(0283) 356039</span>
                
                                    <a href="mailto:kec.kramat.tegal@gmail.com"><span id="et-info-email">kec.kramat.tegal@gmail.com</span></a>
                
                <ul class="et-social-icons">

    <li class="et-social-icon et-social-facebook">
        <a href="#" class="icon">
            <span>Facebook</span>
        </a>
    </li>
    <li class="et-social-icon et-social-twitter">
        <a href="#" class="icon">
            <span>Twitter</span>
        </a>
    </li>
    <li class="et-social-icon et-social-google-plus">
        <a href="#" class="icon">
            <span>Google</span>
        </a>
    </li>
    <li class="et-social-icon et-social-rss">
        <a href="https://www-hclient3.rhcloud.com/feed/" class="icon">
            <span>RSS</span>
        </a>
    </li>

</ul>               </div> <!-- #et-info -->

            
                <div id="et-secondary-menu">
                <div class="et_duplicate_social_icons">
                                <ul class="et-social-icons">

    <li class="et-social-icon et-social-facebook">
        <a href="#" class="icon">
            <span>Facebook</span>
        </a>
    </li>
    <li class="et-social-icon et-social-twitter">
        <a href="#" class="icon">
            <span>Twitter</span>
        </a>
    </li>
    <li class="et-social-icon et-social-google-plus">
        <a href="#" class="icon">
            <span>Google</span>
        </a>
    </li>
    <li class="et-social-icon et-social-rss">
        <a href="https://www-hclient3.rhcloud.com/feed/" class="icon">
            <span>RSS</span>
        </a>
    </li>

</ul>
                            </div><ul id="et-secondary-nav" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-230"><a href="https://www-hclient3.rhcloud.com/login/"><i class="fa fa-lock"></i> Masuk</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-643"><a href="http://tegalkab.go.id"><i class="fa fa-globe"></i> Website Pemerintah Kabupaten Tegal</a></li>
</ul>               </div> <!-- #et-secondary-menu -->

            </div> <!-- .container -->
        </div> <!-- #top-header -->
    
        <header id="main-header" data-height-onload="66">
            <div class="container clearfix et_menu_container">
                            <div class="logo_container">
                    <span class="logo_helper"></span>
                        <a href="{{url('/')}}" title="Situs Resmi Kecamatan Kramat Kabupaten Tegal - Jawa Tengah">
                            <div class="logo">
                                    <img src="/frontend/web-resmi-kecamatan-kramat-kabupaten-tegal/logo.png" alt="Situs Resmi Kecamatan Kramat Kabupaten Tegal - Jawa Tengah" id="logo" />
                            </div>
                        </a>
                        <div class="header-title">
                            <a href="{{url('/')}}" title="Situs Resmi Kecamatan Kramat Kabupaten Tegal - Jawa Tengah">
                                <h4>Pemerintah Kecamatan Kramat Kabupaten Tegal</h4>
                                <address>
                                    Alamat : Jl. Garuda I Kemantran, Kramat, Kabupaten Tegal, Telp : (0283) 356039 <br> Kode POS (range) : 52181                                </address>
                            </a>
                            <div class="newsticker">
                                <script>
(function($) {    
    $(document).ready(function() {
        $('#newsticker_0').newsticker({
                            'tickerTitle' : "",
                            'style' : "scroll",
                            'pauseOnHover' : true,
                            'showControls' : false,
                            'autoStart' : true,
                            'scrollSpeed' : "15",
                            'slideSpeed' : "1000",
                            'slideEasing' : "swing"                        
        });
    });    
})( jQuery );
</script>
<ul class="newsticker" id="newsticker_0">
                    <li>- <a href="https://www-hclient3.rhcloud.com/2015/08/27/desa-tertinggi-kabupaten-tegal/">Desa Tertinggi Kabupaten Tegal</a> </li>
                    <li>- <a href="https://www-hclient3.rhcloud.com/2015/08/27/edit-postingan-dengan-front-end-editor/">Edit Postingan dengan Front End Editor</a> </li>
                    <li>- <a href="https://www-hclient3.rhcloud.com/2015/08/26/tes-penggunaan-editor-baru/">Tes Penggunaan Editor Baru</a> </li>
                    <li>- <a href="https://www-hclient3.rhcloud.com/2015/07/29/10-trik-melawan-malas-bangun-pagi/">10 Trik Melawan Malas Bangun Pagi</a> </li>
                    <li>- <a href="https://www-hclient3.rhcloud.com/2015/07/29/kawasan-konservasi-karang-jeruk-keanekaragaman-hayati-di-kecamatan-kramat/">Kawasan Konservasi Karang Jeruk - Keanekaragaman Hayati di Kecamatan Kramat</a> </li>
    </ul>
                                    </div>
                            
                        </div>

                </div>


@include('themes.divi.mainmenu')
            

            </div> <!-- .container -->
            <div class="et_search_outer">
                <div class="container et_search_form_container">
                    <form role="search" method="get" class="et-search-form" action="{{url('/')}}">
                    <input type="search" class="et-search-field" placeholder="Cari &hellip;" value="" name="s" title="Mencari:" />                  </form>
                    <span class="et_close_search_field"></span>
                </div>
            </div>
        </header> <!-- #main-header -->
