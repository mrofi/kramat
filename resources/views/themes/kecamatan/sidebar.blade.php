<div id="text-2" class="et_pb_widget widget_text">
    <div class="textwidget">
        <iframe width="100%" height="100" 
scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/28467968&auto_play=false&show_comments=false&visual=true"></iframe>
    </div>
</div> <!-- end .et_pb_widget -->
<?php $latest = isset($latest) ? $latest : get('article', null, 10, [], ['id', 'title', 'slug'], 'desc', 'published_at'); ?>
@if (count($latest))
<aside class='widget nlposts-widget'>
        <div class='nlposts-container nlposts-ulist-container nlp-instance-3'><h2 class='nlposts-ulist-wtitle'>Berita Terbaru</h2><ul class='nlposts-wrapper nlposts-ulist nav nav-tabs nav-stacked'>
        @foreach ($latest as $p)
        <li class='nlposts-ulist-litem nlposts-siteid-1'>
        <div class='nlposts-caption'><h3 class='nlposts-ulist-title'><a href='{{$p->url}}'>{{$p->title}}</a></h3>
        </div>
        </li>
        @endforeach
        </ul>
    </div>
</aside>
@endif
<aside class='widget nlposts-widget'>
        <div class='nlposts-container nlposts-block-container nlp-instance-2'><h2 class='nlposts-block-wtitle'>Seputar Kramat</h2>
        <div class='nlposts-wrapper nlposts-block content'>
        <div class='nlposts-block-item nlposts-siteid-2'><ul class='nlposts-block-thumbnail thumbnails'>
        <li class='nlposts-block-thumbnail-litem span3'><a href='https://www-hclient3.rhcloud.com/padaharja/2015/08/09/kegiatan-pkk-desa-padaharja/'><img width="400" height="250" src="https://www-hclient3.rhcloud.com/wp-content/uploads/sites/2/2015/08/13869-400x250.jpg" class=" wp-post-image" alt="Kegiatan PKK Desa Padaharja" title="Kegiatan PKK Desa Padaharja" /></a>
        <div class='nlposts-caption'><h3 class='nlposts-block-title'><a href='https://www-hclient3.rhcloud.com/padaharja/2015/08/09/kegiatan-pkk-desa-padaharja/'>Kegiatan PKK Desa Padaharja</a></h3>
        </div>
        </li></ul>
        </div>
        <div class='nlposts-block-item nlposts-siteid-2'><ul class='nlposts-block-thumbnail thumbnails'>
        <li class='nlposts-block-thumbnail-litem span3'><a href='https://www-hclient3.rhcloud.com/padaharja/2015/08/09/pendapatan-objek-wisata-purwahamba-indah-turun-imbas-perbaikan-jalan/'><img width="370" height="250" src="https://www-hclient3.rhcloud.com/wp-content/uploads/sites/2/2015/08/Purwahamba-Indah.jpg" class=" wp-post-image" alt="Pendapatan Objek Wisata Purwahamba Indah Turun Imbas Perbaikan Jalan" title="Pendapatan Objek Wisata Purwahamba Indah Turun Imbas Perbaikan Jalan" /></a>
        <div class='nlposts-caption'><h3 class='nlposts-block-title'><a href='https://www-hclient3.rhcloud.com/padaharja/2015/08/09/pendapatan-objek-wisata-purwahamba-indah-turun-imbas-perbaikan-jalan/'>Pendapatan Objek Wisata Purwahamba Indah Turun Imbas Perbaikan Jalan</a></h3>
        </div>
        </li></ul>
        </div>
        <div class='nlposts-block-item nlposts-siteid-2'><ul class='nlposts-block-thumbnail thumbnails'>
        <li class='nlposts-block-thumbnail-litem span3'><a href='https://www-hclient3.rhcloud.com/padaharja/2015/08/09/dua-dirjen-beri-ijin-pembuatan-gardu-jaga-terealisasi-2/'><img src='//placehold.it/400x400&text=Dua Dirjen Beri Ijin, Pembuatan Gardu Jaga Terealisasi' alt='Dua Dirjen Beri Ijin, Pembuatan Gardu Jaga Terealisasi' title='Dua Dirjen Beri Ijin, Pembuatan Gardu Jaga Terealisasi' /></a>
        <div class='nlposts-caption'><h3 class='nlposts-block-title'><a href='https://www-hclient3.rhcloud.com/padaharja/2015/08/09/dua-dirjen-beri-ijin-pembuatan-gardu-jaga-terealisasi-2/'>Dua Dirjen Beri Ijin, Pembuatan Gardu Jaga Terealisasi</a></h3>
        </div>
        </li></ul>
        </div>
        </div>
        </div></aside>   