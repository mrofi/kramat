@extends('themes.kecamatan.template')

@section('content')

<div id="page-container">

@include('themes.kecamatan.partials.header')

    <div id="et-main-area">

        <div id="main-content">

@include('themes.kecamatan.partials.content')

        </div> <!-- #main-content -->

@include('themes.kecamatan.partials.footer')

    </div> <!-- #et-main-area -->

</div> <!-- #page-container -->

@endsection