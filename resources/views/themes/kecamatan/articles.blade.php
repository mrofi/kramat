@extends(theme('front'))

@section('content')

<div class="et_pb_posts left-image">

    @include(theme('front', 'partials.articles'))

</div>        
    
@stop

@section('script')
<script>
    $(function() {
        $('.articles').jscroll();
    });
</script>
@stop
